# Android (Java) exchange API wrapper ([Changenow.io](https://changenow.io/))


## How to use
Minimal supported Android version is 4.3:
```
minSdkVersion 18
```

Gradle:
```gradle
dependencies {
  implementation 'io.changenow.changenowapi:java-api:1.0.0'
}
```

Use ```ChangenowApi``` class to interact with all available actions in API.

To register API key, contact us at [api@changenow.io](mailto:api@changenow.io).

## Exchange flow
All API methods wrapped by only the one library method ```ChangenowApi.apiWrapper``` (or its async variant ```ChangenowApi.apiWrapperAsync```). Which has two parameters ```callName``` and ```callParameters```.<br/><br/>
```callName``` - one of the API calls, which you want to request.<br/>
```callParameters``` - if the ```callName``` has required parameter, you need to pass it here. Use ```CallParameters.Builder()``` for passing required parameters.<br/>
(optional) ```apiCallback``` - callback for ```apiWrapperAsync```

### Supported calls and parameters
Expected result | Calls and parameters
------------- | -------------
*List of all available currencies.* | *callName* - CURRENCIES<br/>*callParameters:*<br/>**isActive** - (Optional) Set true to return only active currencies (It's used for Standart flow). <br/>**fixedRate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow
*List of available currencies for a specific currency.* | *callName* - CURRENCIES_TO<br/>*callParameters:*<br/>**ticker** - (Required) Currency ticker.<br/>**fixedRate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow
*Currency info* | *callName* - CURRENCY_INFO<br/>*callParameters:*<br/>**ticker** - (Required) Currency ticker.
*List of transactions* | *callName* - LIST_OF_TRANSACTIONS<br/>*callParameters:*<br/>**apiKey** - (Required) Partner public API key<br/>**tickerFrom** - (Optional) Set a ticker of a payin currency to filter transactions<br/>**tickerTo** - (Optional) Set a ticker of a payout currency to filter transactions<br/>**status** - (Optional) Set a transaction status (available statuses below) to filter transactions<br/>**limit** - (Optional) Limit of transactions to return (default: 10)<br/>**offset** - (Optional) Number of transactions to skip (default: 0)<br/>**dateFrom** - (Optional) Sort transactions by the date of creation. This request returns all transactions created after the specified date. Format: *YYYY-MM-DDTHH:mm:ss.sssZ*<br/>**dateTo** (Optional) Sort transactions by the date of creation. This request returns all transactions created before the specified date. Format: *YYYY-MM-DDTHH:mm:ss.sssZ*
*Transaction status* | *callName* - TX_STATUS<br/>*callParameters:*<br/>**id** - Transaction ID from Create transaction request<br/>**apiKey** - (Required) Partner public API key
*Estimated exchange amount* | *callName* - ESTIMATED<br/>*callParameters:*<br/>**amount** - Amount of funds you want to exchange. <br/>**from** - Ticker of the currency you want to send<br/>**to** - Ticker of the currency you want to receive<br/>**apiKey** - (Required) Partner public API key<br/>**fixedRate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow
*Create exchange transaction* | *callName* - CREATE_TX<br/>*callParameters:*<br/>**apiKey** - (Required) Partner public API key.<br/>**from** - (Required) Ticker of a currency you want to send.<br/>**to** - (Required) Ticker of a currency you want to receive.<br/>**address** - (Required) Address to receive a currency.<br/>**amount** - (Required) Amount you want to exchange.<br/>**extraId** - (Optional) Extra ID for currencies that require it.<br/>**refundAddress** - (Optional) Refund address.<br/>**refundExtraId** - (Optional) Refund Extra ID.<br/>**userId** - (Optional) Partner user ID.<br/>**payload** - (Optional) Object that can contain up to 5 arbitrary fields up to 64 characters long.<br/>**contactEmail** - (Optional) Your contact email for notification in case something goes wrong with your exchange<br/>**fixedRate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow
 *List of all available pairs* | *callName* - PAIRS<br/>*callParameters:*<br/>**includePartners** - Set false to return all available pairs, except pairs supported by our partners.<br/>**fixedRate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow<br/>**IMPORTANT:** There are different response types for fixed rate and standard flows (```List<FixRateMarket>``` - for fixed rate flow and ```List<String>``` - for standard flow).
 *Minimal exchange amount* | *callName* - MIN_AMOUNT<br/>*callParameters:*<br/>**from** - Ticker of the currency you want to send.<br/>**to** - Ticker of the currency you want to receive.

### Full version of API documentation
All methods described here ([API Documentation](https://changenow.io/api/docs)).

## Sample app
See how to use the the library in your project:
[Sample app folder](/app)

## Any questions
If you would like to contribute code you can do so through GitHub by forking the repository and sending a pull request.

If you have any question create an issue.

## License
Library are licensed under the [GPL-3.0 License](LICENSE).

Enjoy!
