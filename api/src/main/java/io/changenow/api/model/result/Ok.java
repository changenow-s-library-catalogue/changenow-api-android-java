package io.changenow.api.model.result;

import java.util.NoSuchElementException;

class Ok<T> implements Result<T> {

    private final T value;

    Ok(final T value) {
        this.value = value;
    }

    @Override
    public boolean isOk() {
        return true;
    }

    @Override
    public Throwable getError() {
        throw new NoSuchElementException("Result contains a value: " + value.toString());
    }

    @Override
    public T get() {
        return value;
    }

    @Override
    public String toString() {
        return "Ok{" +
                "value=" + value +
                '}';
    }

}
