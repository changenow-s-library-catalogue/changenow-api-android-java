package io.changenow.api.model.responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samosudovd on 07/06/2018.
 */

public class Estimated {

        @Expose
        @SerializedName("estimatedAmount")
        private float estimatedAmount;
        @Expose
        @SerializedName("networkFee")
        private float networkFee;
        @Expose
        @SerializedName("transactionSpeedForecast")
        private String transactionSpeedForecast;
        @Expose
        @SerializedName("warningMessage")
        private String warningMessage;

        public float getEstimatedAmount() {
                return estimatedAmount;
        }

        public void setEstimatedAmount(float estimatedAmount) {
                this.estimatedAmount = estimatedAmount;
        }

        public float getNetworkFee() {
                return networkFee;
        }

        public void setNetworkFee(float networkFee) {
                this.networkFee = networkFee;
        }

        public String getTransactionSpeedForecast() {
                return transactionSpeedForecast;
        }

        public void setTransactionSpeedForecast(String transactionSpeedForecast) {
                this.transactionSpeedForecast = transactionSpeedForecast;
        }

        public String getWarningMessage() {
                return warningMessage;
        }

        public void setWarningMessage(String warningMessage) {
                this.warningMessage = warningMessage;
        }

        @Override
        public String toString() {
                return "Estimated{" +
                        "estimatedAmount=" + estimatedAmount +
                        ", networkFee=" + networkFee +
                        ", transactionSpeedForecast='" + transactionSpeedForecast + '\'' +
                        ", warningMessage='" + warningMessage + '\'' +
                        '}';
        }
}
