package io.changenow.api.model;

/**
 * Created by samosudovd on 07/06/2018.
 */

public enum CallName {
        CURRENCIES, CURRENCIES_TO, CURRENCY_INFO, LIST_OF_TRANSACTIONS, TX_STATUS,
    ESTIMATED, CREATE_TX, PAIRS, MIN_AMOUNT
}
