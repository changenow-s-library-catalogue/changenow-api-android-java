package io.changenow.api.model.result;

public interface ApiCallback<T> {

    void onResult(T result);

}
