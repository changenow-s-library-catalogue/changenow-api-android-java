package io.changenow.api.model.result;


import androidx.annotation.Nullable;

public final class CallParameters {

    final boolean isActive;
    final boolean fixedRate;
    final @Nullable String tickerInfo;
    final @Nullable String tickerFrom;
    final @Nullable String tickerTo;
    final @Nullable String txStatus;
    final int limit;
    final int offset;
    @Nullable final String txId;
    final String amount;
    final String address;
    final String extraId;
    final String refundAddress;
    final String refundExtraId;
    final String userId;
    final String payload;
    final String contactEmail;
    final boolean includePartners;
    final String dateFrom;
    final String dateTo;
    
    public CallParameters() {
        this(new Builder());
    }

    public CallParameters(Builder builder) {
        this.isActive = builder.isActive;
        this.fixedRate = builder.fixedRate;
        this.tickerInfo = builder.tickerInfo;
        this.tickerFrom = builder.tickerFrom;
        this.tickerTo = builder.tickerTo;
        this.txStatus = builder.txStatus;
        this.limit = builder.limit;
        this.offset = builder.offset;
        this.txId = builder.txId;
        this.amount = builder.amount;
        this.address = builder.address;
        this.extraId = builder.extraId;
        this.refundAddress = builder.refundAddress;
        this.refundExtraId = builder.refundExtraId;
        this.userId = builder.userId;
        this.payload = builder.payload;
        this.contactEmail = builder.contactEmail;
        this.includePartners = builder.includePartners;
        this.dateFrom = builder.dateFrom;
        this.dateTo = builder.dateTo;
    }

    public static final class Builder {
        boolean isActive;
        boolean fixedRate;
        @Nullable String tickerInfo;
        @Nullable String tickerFrom;
        @Nullable String tickerTo;
        @Nullable String txStatus;
        int limit;
        int offset;
        @Nullable String txId;
        @Nullable String amount;
        @Nullable String address;
        @Nullable String extraId;
        @Nullable String refundAddress;
        @Nullable String refundExtraId;
        @Nullable String userId;
        @Nullable String payload;
        @Nullable String contactEmail;
        boolean includePartners;
        @Nullable String dateFrom;
        @Nullable String dateTo;

        public Builder() {
            isActive = false;
            fixedRate = false;
            limit = 50;
            offset = 0;
            includePartners = false;
        }

        public CallParameters build() {
            return new CallParameters(this);
        }

        public Builder isActive(boolean isActive) {
            this.isActive = isActive;
            return this;
        }

        public Builder fixedRate(boolean fixedRate) {
            this.fixedRate = fixedRate;
            return this;
        }

        public Builder tickerInfo(String tickerInfo) {
            this.tickerInfo = tickerInfo;
            return this;
        }

        public Builder tickerFrom(String tickerFrom) {
            this.tickerFrom = tickerFrom;
            return this;
        }

        public Builder tickerTo(String tickerTo) {
            this.tickerTo = tickerTo;
            return this;
        }

        public Builder txStatus(String txStatus) {
            this.txStatus = txStatus;
            return this;
        }

        public Builder limit(int limit) {
            this.limit = limit;
            return this;
        }

        public Builder offset(int offset) {
            this.offset = offset;
            return this;
        }

        public Builder txId(String txId) {
            this.txId = txId;
            return this;
        }

        public Builder amount(String amount) {
            this.amount = amount;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder extraId(String extraId) {
            this.extraId = extraId;
            return this;
        }
        public Builder refundAddress(String refundAddress) {
            this.refundAddress = refundAddress;
            return this;
        }

        public Builder refundExtraId(String refundExtraId) {
            this.refundExtraId = refundExtraId;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder payload(String payload) {
            this.payload = payload;
            return this;
        }
        public Builder contactEmail(String contactEmail) {
            this.contactEmail = contactEmail;
            return this;
        }

        public Builder includePartners(boolean includePartners) {
            this.includePartners = includePartners;
            return this;
        }

        public Builder dateFrom(String dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public Builder dateTo(String dateTo) {
            this.dateTo = dateTo;
            return this;
        }

    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isFixedRate() {
        return fixedRate;
    }

    public String getTickerInfo() {
        return tickerInfo;
    }

    public String getTickerFrom() {
        return tickerFrom;
    }

    public String getTickerTo() {
        return tickerTo;
    }

    @Nullable
    public String getTxStatus() {
        return txStatus;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    @Nullable
    public String getTxId() {
        return txId;
    }

    public String getAmount() {
        return amount;
    }

    public String getAddress() {
        return address;
    }

    public String getExtraId() {
        return extraId;
    }

    public String getRefundAddress() {
        return refundAddress;
    }

    public String getRefundExtraId() {
        return refundExtraId;
    }

    public String getUserId() {
        return userId;
    }

    public String getPayload() {
        return payload;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public boolean isIncludePartners() {
        return includePartners;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }
}
