package io.changenow.api.model.responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samosudovd on 07/06/2018.
 */

public class TxItem {

        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("hash")
        private String hash;
        @Expose
        @SerializedName("payinHash")
        private String payinHash;
        @Expose
        @SerializedName("payoutHash")
        private String payoutHash;
        @Expose
        @SerializedName("payinAddress")
        private String payinAddress;
        @Expose
        @SerializedName("payoutAddress")
        private String payoutAddress;
        @Expose
        @SerializedName("payinExtraId")
        private String payinExtraId;
        @Expose
        @SerializedName("payoutExtraId")
        private String payoutExtraId;
        @Expose
        @SerializedName("fromCurrency")
        private String fromCurrency;
        @Expose
        @SerializedName("toCurrency")
        private String toCurrency;
        @Expose
        @SerializedName("amountSend")
        private String amountSend;
        @Expose
        @SerializedName("amountReceive")
        private String amountReceive;
        @Expose
        @SerializedName("networkFee")
        private String networkFee;
        @Expose
        @SerializedName("updatedAt")
        private String updatedAt;
        @Expose
        @SerializedName("expectedSendAmount")
        private String expectedSendAmount;
        @Expose
        @SerializedName("expectedReceiveAmount")
        private String expectedReceiveAmount;
        @Expose
        @SerializedName("refundAddress")
        private String refundAddress;
        @Expose
        @SerializedName("refundExtraId")
        private String refundExtraId;

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public String getHash() {
                return hash;
        }

        public void setHash(String hash) {
                this.hash = hash;
        }

        public String getPayinHash() {
                return payinHash;
        }

        public void setPayinHash(String payinHash) {
                this.payinHash = payinHash;
        }

        public String getPayoutHash() {
                return payoutHash;
        }

        public void setPayoutHash(String payoutHash) {
                this.payoutHash = payoutHash;
        }

        public String getPayinAddress() {
                return payinAddress;
        }

        public void setPayinAddress(String payinAddress) {
                this.payinAddress = payinAddress;
        }

        public String getPayoutAddress() {
                return payoutAddress;
        }

        public void setPayoutAddress(String payoutAddress) {
                this.payoutAddress = payoutAddress;
        }

        public String getPayinExtraId() {
                return payinExtraId;
        }

        public void setPayinExtraId(String payinExtraId) {
                this.payinExtraId = payinExtraId;
        }

        public String getPayoutExtraId() {
                return payoutExtraId;
        }

        public void setPayoutExtraId(String payoutExtraId) {
                this.payoutExtraId = payoutExtraId;
        }

        public String getFromCurrency() {
                return fromCurrency;
        }

        public void setFromCurrency(String fromCurrency) {
                this.fromCurrency = fromCurrency;
        }

        public String getToCurrency() {
                return toCurrency;
        }

        public void setToCurrency(String toCurrency) {
                this.toCurrency = toCurrency;
        }

        public String getAmountSend() {
                return amountSend;
        }

        public void setAmountSend(String amountSend) {
                this.amountSend = amountSend;
        }

        public String getAmountReceive() {
                return amountReceive;
        }

        public void setAmountReceive(String amountReceive) {
                this.amountReceive = amountReceive;
        }

        public String getNetworkFee() {
                return networkFee;
        }

        public void setNetworkFee(String networkFee) {
                this.networkFee = networkFee;
        }

        public String getUpdatedAt() {
                return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
        }

        public String getExpectedSendAmount() {
                return expectedSendAmount;
        }

        public void setExpectedSendAmount(String expectedSendAmount) {
                this.expectedSendAmount = expectedSendAmount;
        }

        public String getExpectedReceiveAmount() {
                return expectedReceiveAmount;
        }

        public void setExpectedReceiveAmount(String expectedReceiveAmount) {
                this.expectedReceiveAmount = expectedReceiveAmount;
        }

        public String getRefundAddress() {
                return refundAddress;
        }

        public void setRefundAddress(String refundAddress) {
                this.refundAddress = refundAddress;
        }

        public String getRefundExtraId() {
                return refundExtraId;
        }

        public void setRefundExtraId(String refundExtraId) {
                this.refundExtraId = refundExtraId;
        }

        @Override
        public String toString() {
                return "TxItem{" +
                        "id='" + id + '\'' +
                        ", status='" + status + '\'' +
                        ", hash='" + hash + '\'' +
                        ", payinHash='" + payinHash + '\'' +
                        ", payoutHash='" + payoutHash + '\'' +
                        ", payinAddress='" + payinAddress + '\'' +
                        ", payoutAddress='" + payoutAddress + '\'' +
                        ", payinExtraId='" + payinExtraId + '\'' +
                        ", payoutExtraId='" + payoutExtraId + '\'' +
                        ", fromCurrency='" + fromCurrency + '\'' +
                        ", toCurrency='" + toCurrency + '\'' +
                        ", amountSend='" + amountSend + '\'' +
                        ", amountReceive='" + amountReceive + '\'' +
                        ", networkFee='" + networkFee + '\'' +
                        ", updatedAt='" + updatedAt + '\'' +
                        ", expectedSendAmount='" + expectedSendAmount + '\'' +
                        ", expectedReceiveAmount='" + expectedReceiveAmount + '\'' +
                        ", refundAddress='" + refundAddress + '\'' +
                        ", refundExtraId='" + refundExtraId + '\'' +
                        '}';
        }
}
