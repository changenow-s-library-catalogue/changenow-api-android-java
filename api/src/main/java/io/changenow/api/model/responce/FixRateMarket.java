package io.changenow.api.model.responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samosudovd on 07/06/2018.
 */

public class FixRateMarket {

        @Expose
        @SerializedName("from")
        private String from;
        @Expose
        @SerializedName("to")
        private String to;
        @Expose
        @SerializedName("min")
        private float min;
        @Expose
        @SerializedName("max")
        private float max;
        @Expose
        @SerializedName("rate")
        private float rate;
        @Expose
        @SerializedName("minerFee")
        private float minerFee;

        public String getFrom() {
                return from;
        }

        public void setFrom(String from) {
                this.from = from;
        }

        public String getTo() {
                return to;
        }

        public void setTo(String to) {
                this.to = to;
        }

        public float getMin() {
                return min;
        }

        public void setMin(float min) {
                this.min = min;
        }

        public float getMax() {
                return max;
        }

        public void setMax(float max) {
                this.max = max;
        }

        public float getRate() {
                return rate;
        }

        public void setRate(float rate) {
                this.rate = rate;
        }

        public float getMinerFee() {
                return minerFee;
        }

        public void setMinerFee(float minerFee) {
                this.minerFee = minerFee;
        }

        @Override
        public String toString() {
                return "FixRateMarket{" +
                        "from='" + from + '\'' +
                        ", to='" + to + '\'' +
                        ", min=" + min +
                        ", max=" + max +
                        ", rate=" + rate +
                        ", minerFee=" + minerFee +
                        '}';
        }
}
