package io.changenow.api;

import android.accounts.NetworkErrorException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.changenow.api.model.responce.CurrencyItem;
import io.changenow.api.model.responce.Estimated;
import io.changenow.api.model.responce.FixRateMarket;
import io.changenow.api.model.CallName;
import io.changenow.api.model.responce.MinAmount;
import io.changenow.api.model.responce.TxItem;
import io.changenow.api.model.result.ApiCallback;
import io.changenow.api.model.result.CallParameters;
import io.changenow.api.model.result.Result;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ChangenowApi {

    private String apiKey;
    public ChangeNowManager changeNowManager;

    private final static String API_URL = "https://changenow.io/api/v1/";

    private ChangenowApi(String apiKey, ChangeNowManager changeNowManager) {
        this.apiKey = apiKey;
        this.changeNowManager = changeNowManager;
    }

    public static class Builder {

        private String apiKey = "";

        public Builder apiKey(String apiKey) {
            if (apiKey == null || apiKey.isEmpty()) {
                throw new IllegalArgumentException("apiKey must not be empty or null apiKey=" + apiKey);
            }
            this.apiKey = apiKey;
            return this;
        }

        public ChangenowApi build() {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .client(getNewHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson));

            Retrofit retrofit = builder.build();

            ChangeNowManager changeNowManager = retrofit.create(ChangeNowManager.class);
            return new ChangenowApi(this.apiKey, changeNowManager);
        }

        private static OkHttpClient getNewHttpClient() {
            OkHttpClient.Builder client = new OkHttpClient.Builder()
                    .followRedirects(true)
                    .followSslRedirects(true)
                    .retryOnConnectionFailure(true)
                    .cache(null)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS);

            return client.build();
        }
    }

    /** Single method */
    public Result<?> apiWrapper(CallName methodName, CallParameters methodParameters) {
        if (methodName == null) {
            return Result.error(new IllegalArgumentException("methodName must not be null"));
        }

        if (methodParameters == null) {
            return Result.error(new IllegalArgumentException("methodParameters must not be null"));
        }

        if (apiKey == null) {
            return Result.error(new IllegalArgumentException("apiKey required"));
        }

        return pickMethod(methodName, methodParameters);
    }

    public <T> void apiWrapperAsync(CallName methodName, CallParameters methodParameters, ApiCallback<Result<T>> apiCallback) {
        if (methodName == null) {
            apiCallback.onResult(Result.error(new IllegalArgumentException("methodName must not be null")));
            return;
        }

        if (methodParameters == null) {
            apiCallback.onResult(Result.error(new IllegalArgumentException("methodParameters must not be null")));
            return;
        }

        if (apiKey == null) {
            apiCallback.onResult(Result.error(new IllegalArgumentException("apiKey required")));
            return;
        }

        pickMethodCall(methodName, methodParameters, apiCallback);
    }

    private Result<?> pickMethod(CallName methodName, CallParameters methodParameters) {
        Call<?> call = null;

        switch (methodName) {
            case CURRENCIES:
                call = getCurrencies(methodParameters);
                break;
            case CURRENCIES_TO:
                call = getCurrenciesTo(methodParameters);
                break;
            case CURRENCY_INFO:
                call = getCurrencyInfo(methodParameters);
                break;
            case LIST_OF_TRANSACTIONS:
                call = getListOfTransactions(methodParameters);
                break;
            case TX_STATUS:
                call = getTxStatus(methodParameters);
                break;
            case ESTIMATED:
                call = getEstimated(methodParameters);
                break;
            case CREATE_TX:
                call = getCreateTx(methodParameters);
                break;
            case PAIRS:
                call = getPairs(methodParameters);
                break;
            case MIN_AMOUNT:
                call = getMinAmount(methodParameters);
                break;
        }

        if (call != null) {
            try {
                Response<?> response = call.execute();
                return catchResponse(response);
            } catch (IOException error) {
                return Result.error(new IOException("methodName=" + methodName + " error=" + error.getMessage()));
            }
        }

        return Result.error(new NoSuchMethodError("Method not found"));
    }

    private <T> Result<T> pickMethodCall(final CallName methodName, CallParameters methodParameters, final ApiCallback<Result<T>> apiCallback) {
        Call<T> call = null;

        switch (methodName) {
            case CURRENCIES:
                call = getCurrencies(methodParameters);
                break;
            case CURRENCIES_TO:
                call = getCurrenciesTo(methodParameters);
                break;
            case CURRENCY_INFO:
                call = getCurrencyInfo(methodParameters);
                break;
            case LIST_OF_TRANSACTIONS:
                call = getListOfTransactions(methodParameters);
                break;
            case TX_STATUS:
                call = getTxStatus(methodParameters);
                break;
            case ESTIMATED:
                call = getEstimated(methodParameters);
                break;
            case CREATE_TX:
                call = getCreateTx(methodParameters);
                break;
            case PAIRS:
                call = getPairs(methodParameters);
                break;
            case MIN_AMOUNT:
                call = getMinAmount(methodParameters);
                break;
        }

        if (call != null) {
            call.enqueue(new Callback<T>() {
                @Override
                public void onResponse(Call<T> call, Response<T> response) {
                    if (response.isSuccessful()) {
                        apiCallback.onResult(Result.ok(response.body()));
                    } else {
                        apiCallback.onResult(Result.error(new NetworkErrorException("Response is not successful error=" + response.message())));
                    }
                }

                @Override
                public void onFailure(Call<T> call, Throwable t) {
                    apiCallback.onResult(Result.error(new IOException("methodName=" + methodName + " error=" + t.getMessage())));
                }
            });
        }

        return Result.error(new NoSuchMethodError("Method not found"));
    }

    private <T> Call<T> getCurrencies(CallParameters methodParameters) {
        return (Call<T>) changeNowManager.currencies(
                methodParameters.isActive(),
                methodParameters.isFixedRate());
    }

    private <T> Call<T> getCurrenciesTo(CallParameters methodParameters) throws IllegalArgumentException {
        if (methodParameters.getTickerInfo() == null) {
            throw new IllegalArgumentException("tickerInfo required");
        }
        return (Call<T>) changeNowManager.currenciesTo(
                methodParameters.getTickerInfo(),
                methodParameters.isFixedRate());
    }

    private <T> Call<T> getCurrencyInfo(CallParameters methodParameters) throws IllegalArgumentException {
        if (methodParameters.getTickerInfo() == null) {
            throw new IllegalArgumentException("tickerInfo required");
        }
        return (Call<T>) changeNowManager.currencyInfo(
                methodParameters.getTickerInfo());
    }

    private <T> Call<T> getListOfTransactions(CallParameters methodParameters) {
        return (Call<T>) changeNowManager.listOfTransactions(
                apiKey,
                methodParameters.getTickerFrom(),
                methodParameters.getTickerTo(),
                methodParameters.getTxStatus(),
                methodParameters.getLimit(),
                methodParameters.getOffset(),
                methodParameters.getDateFrom(),
                methodParameters.getDateTo());
    }

    private <T> Call<T> getTxStatus(CallParameters methodParameters) throws IllegalArgumentException {
        if (methodParameters.getTxId() == null) {
            throw new IllegalArgumentException("txId required");
        }
        return (Call<T>) changeNowManager.txStatus(
                methodParameters.getTxId(),
                apiKey);
    }

    private <T> Call<T> getEstimated(CallParameters methodParameters) throws IllegalArgumentException {
        if (methodParameters.getAmount() == null) {
            throw new IllegalArgumentException("amount required");
        }
        if (methodParameters.getTickerFrom() == null) {
            throw new IllegalArgumentException("tickerFrom required");
        }
        if (methodParameters.getTickerTo() == null) {
            throw new IllegalArgumentException("tickerTo required");
        }
        if (methodParameters.isFixedRate()) {
            return (Call<T>) changeNowManager.estimatedFixes(
                    methodParameters.getAmount(),
                    methodParameters.getTickerFrom(),
                    methodParameters.getTickerTo(),
                    apiKey);
        } else {
            return (Call<T>) changeNowManager.estimated(
                    methodParameters.getAmount(),
                    methodParameters.getTickerFrom(),
                    methodParameters.getTickerTo(),
                    apiKey);
        }
    }

    private <T> Call<T> getCreateTx(CallParameters methodParameters) throws IllegalArgumentException {
        if (methodParameters.getTickerFrom() == null) {
            throw new IllegalArgumentException("tickerFrom required");
        }
        if (methodParameters.getTickerTo() == null) {
            throw new IllegalArgumentException("tickerTo required");
        }
        if (methodParameters.getAddress() == null) {
            throw new IllegalArgumentException("address required");
        }
        if (methodParameters.getAmount() == null) {
            throw new IllegalArgumentException("amount required");
        }

        if (methodParameters.isFixedRate()) {
            return (Call<T>) changeNowManager.createFixedTx(
                    apiKey,
                    methodParameters.getTickerFrom(),
                    methodParameters.getTickerTo(),
                    methodParameters.getAddress(),
                    methodParameters.getAmount(),
                    methodParameters.getExtraId(),
                    methodParameters.getRefundAddress(),
                    methodParameters.getRefundExtraId(),
                    methodParameters.getUserId(),
                    methodParameters.getPayload(),
                    methodParameters.getContactEmail());
        } else {
            return (Call<T>) changeNowManager.createTx(
                    apiKey,
                    methodParameters.getTickerFrom(),
                    methodParameters.getTickerTo(),
                    methodParameters.getAddress(),
                    methodParameters.getAmount(),
                    methodParameters.getExtraId(),
                    methodParameters.getRefundAddress(),
                    methodParameters.getRefundExtraId(),
                    methodParameters.getUserId(),
                    methodParameters.getPayload(),
                    methodParameters.getContactEmail());
        }

    }

    private <T> Call<T> getPairs(CallParameters methodParameters) {
        if (methodParameters.isFixedRate()) {
            return (Call<T>) changeNowManager.listOfFixed(apiKey);
        } else {
            return (Call<T>) changeNowManager.pairs(
                    methodParameters.isIncludePartners());
        }
    }

    private <T> Call<T> getMinAmount(CallParameters methodParameters) throws IllegalArgumentException {
        if (methodParameters.getTickerFrom() == null) {
            throw new IllegalArgumentException("tickerFrom required");
        }
        if (methodParameters.getTickerTo() == null) {
            throw new IllegalArgumentException("tickerTo required");
        }
        return (Call<T>) changeNowManager.minAmount(
                methodParameters.getTickerFrom(),
                methodParameters.getTickerTo());
    }

    private Result<?> catchResponse(Response<?> response) {
        if (response.isSuccessful()) {
            return Result.ok(response.body());
        } else {
            return Result.error(new NetworkErrorException("API error: " + response.errorBody()));
        }
    }

    public interface ChangeNowManager {

        /** Common methods */

        @GET("currencies")
        Call<List<CurrencyItem>> currencies(@Query("active") boolean isActive,
                                            @Query("fixedRate") boolean fixedRate);

        @GET("currencies-to/{ticker}")
        Call<List<CurrencyItem>> currenciesTo(@Path("ticker") String ticker,
                                              @Query("fixedRate") boolean fixedRate);

        @GET("currencies/{ticker}")
        Call<CurrencyItem> currencyInfo(@Path("ticker") String ticker);

        @GET("transactions/{apiKey}")
        Call<List<TxItem>> listOfTransactions(@Path("apiKey") String apiKey,
                                              @Query("from") String tickerFrom,
                                              @Query("to") String tickerTo,
                                              @Query("status") String status,
                                              @Query("limit") int limit,
                                              @Query("offset") int offset,
                                              @Query("dateFrom") String dateFrom,
                                              @Query("dateTo") String dateTo);

        @GET("transactions/{id}/{apikey}")
        Call<TxItem> txStatus(@Path("id") String id,
                              @Path("apikey") String apikey);

        /** Standard Flow (Floating Rate) */

        @GET("exchange-amount/{amount}/{from}_{to}")
        Call<Estimated> estimated(@Path("amount") String amount,
                                  @Path("from") String from,
                                  @Path("to") String to,
                                  @Query("api_key") String apikKey);

        @POST("transactions/{apiKey}")
        @FormUrlEncoded
        Call<TxItem> createTx(@Path("apiKey") String apiKey,
                              @Field("from") String from,
                              @Field("to") String to,
                              @Field("address") String address,
                              @Field("amount") String amount,
                              @Field("extraId") String extraId,
                              @Field("refundAddress") String refundAddress,
                              @Field("refundExtraId") String refundExtraId,
                              @Field("userId") String userId,
                              @Field("payload") String payload,
                              @Field("contactEmail") String contactEmail);

        @GET("market-info/available-pairs")
        Call<List<String>> pairs(@Query("includePartners") boolean includePartners);

        @GET("min-amount/{from}_{to}")
        Call<MinAmount> minAmount(@Path("from") String from,
                                  @Path("to") String to);

        /** Fixed-Rate Flow */

        @GET("market-info/fixed-rate/{apiKey}")
        Call<List<FixRateMarket>> listOfFixed(@Path("apiKey") String apiKey);

        @GET("exchange-amount/fixed-rate/{sendAmount}/{from}_{to}")
        Call<Estimated> estimatedFixes(@Path("sendAmount") String sendAmount,
                                       @Path("from") String from,
                                       @Path("to") String to,
                                       @Query("api_key") String apiKey);

        @POST("transactions/fixed-rate/{apiKey}")
        @FormUrlEncoded
        Call<TxItem> createFixedTx(@Path("apiKey") String apiKey,
                                   @Field("from") String from,
                                   @Field("to") String to,
                                   @Field("address") String address,
                                   @Field("amount") String amount,
                                   @Field("extraId") String extraId,
                                   @Field("refundAddress") String refundAddress,
                                   @Field("refundExtraId") String refundExtraId,
                                   @Field("userId") String userId,
                                   @Field("payload") String payload,
                                   @Field("contactEmail") String contactEmail);

    }

    public String getApiKey() {
        return apiKey;
    }
}
