package io.changenow.changenowapi.ui.common_api;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import io.changenow.changenowapi.R;

public class CommonApiFragment extends Fragment {

    private CommonApiViewModel commonApiViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        commonApiViewModel =
                ViewModelProviders.of(this).get(CommonApiViewModel.class);
        View root = inflater.inflate(R.layout.fragment_common, container, false);

        final SwitchCompat switchFixed = root.findViewById(R.id.switch_fixed);

        final Button buttonCurrencyList = root.findViewById(R.id.btn_currency_list);
        final Button buttonCurrencyTo = root.findViewById(R.id.btn_currency_to);
        final Button buttonCurrencyInfo = root.findViewById(R.id.btn_currency_info);
        final Button buttonTxList = root.findViewById(R.id.btn_tx_list);
        final Button buttonTxStatus = root.findViewById(R.id.btn_tx_status);
        final Button btnEstimated = root.findViewById(R.id.btn_estimated);
        final Button btnCreateTx = root.findViewById(R.id.btn_create_tx);
        final Button btnPairs = root.findViewById(R.id.btn_pairs);
        final Button btnMinAmount = root.findViewById(R.id.btn_min_amount);

        final TextView textResponse = root.findViewById(R.id.text_response);

        buttonCurrencyList.setOnClickListener(v -> commonApiViewModel.getCurrencies(switchFixed.isChecked()));
        buttonCurrencyTo.setOnClickListener(v -> commonApiViewModel.currencyTo(switchFixed.isChecked()));
        buttonCurrencyInfo.setOnClickListener(v -> commonApiViewModel.currencyInfo());
        buttonTxList.setOnClickListener(v -> commonApiViewModel.listOfTransactions());
        buttonTxStatus.setOnClickListener(v -> commonApiViewModel.txStatus());
        btnEstimated.setOnClickListener(v -> commonApiViewModel.estimated(switchFixed.isChecked()));
        btnCreateTx.setOnClickListener(v -> commonApiViewModel.createTx(switchFixed.isChecked()));
        btnPairs.setOnClickListener(v -> commonApiViewModel.pairs(switchFixed.isChecked()));
        btnMinAmount.setOnClickListener(v -> commonApiViewModel.minAmount());

        commonApiViewModel.getResponseData().observe(getViewLifecycleOwner(), textResponse::setText);

        return root;
    }


}
