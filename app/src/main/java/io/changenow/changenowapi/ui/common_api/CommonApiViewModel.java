package io.changenow.changenowapi.ui.common_api;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.changenow.api.ChangenowApi;
import io.changenow.api.model.CallName;
import io.changenow.api.model.responce.MinAmount;
import io.changenow.api.model.result.CallParameters;

import static io.changenow.changenowapi.MainActivity.API_KEY;

public class CommonApiViewModel extends ViewModel {

    private MutableLiveData<String> responseData;
    private ChangenowApi changenowApi;
    private Gson gson = new Gson();

    public CommonApiViewModel() {
        responseData = new MutableLiveData<>();
        changenowApi = new ChangenowApi.Builder().apiKey(API_KEY).build();
    }

    void getCurrencies(boolean isFixedRate) {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.CURRENCIES,
                new CallParameters.Builder()
                        .fixedRate(isFixedRate)
                        .build(),
                (result) -> responseData.setValue(gson.toJson(result.get()))
        );
    }

    void currencyTo(boolean isFixedRate) {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.CURRENCIES_TO,
        new CallParameters.Builder()
                .tickerInfo("btc")
                .fixedRate(isFixedRate)
                .build(),
        (result) -> responseData.setValue("BTC:\n" + gson.toJson(result.get())));
    }

    void currencyInfo() {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.CURRENCY_INFO,
                new CallParameters.Builder()
                        .tickerInfo("btc")
                        .build(),
                (result) -> responseData.setValue("BTC:\n" + gson.toJson(result.get())));
    }

    void listOfTransactions() {
        responseData.setValue("");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String dateFrom = dateFormat.format(new Date(1587556824L));
        String dateTo = dateFormat.format(new Date());
        Log.d("tag", "listOfTransactions dateFrom=" + dateFrom + " dateTo=" + dateTo);
        CallParameters parameters = new CallParameters.Builder()
                .tickerFrom("btc")
                .tickerTo("eth")
                .dateFrom(dateFrom)
                .dateTo(dateTo)
                .build();
        changenowApi.apiWrapperAsync(CallName.LIST_OF_TRANSACTIONS,
                parameters,
                (result) -> responseData.setValue("BTC - ETH:\n" + gson.toJson(result.get())));
    }

    void txStatus() {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.TX_STATUS,
                new CallParameters.Builder()
                        .txId("335e0ddcb2aa11")
                        .build(),
                (result) -> responseData.setValue(gson.toJson(result.get())));
    }

    void estimated(boolean isFixedRate) {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.ESTIMATED,
                new CallParameters.Builder()
                        .amount("0.003")
                        .tickerFrom("btc")
                        .tickerTo("eth")
                        .fixedRate(isFixedRate)
                        .build(),
                (result) -> responseData.setValue("BTC - ETH:\n" + gson.toJson(result.get())));
    }

    void createTx(boolean isFixedRate) {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.CREATE_TX,
                new CallParameters.Builder()
                        .tickerFrom("btc")
                        .tickerTo("eth")
                        .address("0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE")
                        .amount("0.003")
                        .fixedRate(isFixedRate)
                        .build(),
                (result) -> responseData.setValue(gson.toJson(result.get())));
    }

    void pairs(boolean isFixedRate) {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.PAIRS,
                new CallParameters.Builder()
                        .fixedRate(isFixedRate)
                        .build(),
                (result) -> responseData.setValue(gson.toJson(result.get())));
    }

    void minAmount() {
        responseData.setValue("");
        changenowApi.apiWrapperAsync(CallName.MIN_AMOUNT,
                new CallParameters.Builder()
                        .tickerFrom("btc")
                        .tickerTo("eth")
                        .build(),
                (result) -> responseData.setValue("BTC - ETH:\n" +
                        String.format("%.9f", ((MinAmount) result.get()).getMinAmount())));
    }

    LiveData<String> getResponseData() {
        return responseData;
    }
}